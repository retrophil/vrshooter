﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyComponent : VRGameJamApp
{
     public float speed;
     float accuracy = 8;
     Transform goal;
     public float enemyHealth = 3;
     static float timeHits = 0;
     bool hit = false;

    void Start()
    {   
        goal = Camera.main.GetComponent<Camera>().transform;
        this.transform.LookAt(goal.position);
        Invoke("PlayStep",1);
    }

    void PlayStep()
    {
        GetComponentInChildren<AudioSource>().Play();
        Invoke("PlayStep",1);
    }

    void Update()
    {
        if (Vector3.Distance(transform.position,goal.position) > accuracy)
        {
            if (!hit)
            {
                transform.Translate(Vector3.forward*Time.deltaTime*speed);        
            }
            else
            {
                GetComponent<Animator>().SetTrigger("hit");
                hit = false;
            }
        }
        else
        {
            timeHits += Time.deltaTime;
            if (timeHits > 2)
            {
                DrainPlayersHealth();    
            }
        }
    }

    void DrainPlayersHealth()
    {
        if (app.model.health > 0)
        {
            app.controller.SetHealth(0.1f);
            timeHits = 0;    
        }
        else
        {
            if (app.controller.gameState == VRController.GameState.TITLE)
            {
                SceneManager.LoadScene("VRShooter");
            }
            else if (app.controller.gameState == VRController.GameState.PLAYING)
            {
                app.controller.gameState = VRController.GameState.DEAD;
                app.controller.GameLoop(VRController.GameState.DEAD);   
            }
             
        }
    }

        void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            enemyHealth--;
            hit = true;
            
            app.controller.SetScore(100);
            

            if (enemyHealth <= 0)
            {
                Kill(transform.gameObject);
            }
        }    
    }

    void Kill(GameObject obj)
    {
        obj.GetComponent<Animator>().SetBool("dead",true);
        obj.GetComponent<BoxCollider>().enabled = false;
        Destroy(obj,1);
    } 
}
