﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRModel : MonoBehaviour
{
    public int score = 0;
    public float health = 1;

    public GameObject titleObj;
    public GameObject healthObj;
    public GameObject scoreObj;
    public GameObject gunObj;
    public GameObject enemiesObj;
    public GameObject game;
    public GameObject yourScoreTitle;
    public GameObject yourScoreInt;
    public GameObject ScoreInt;
    public GameObject ScoreTitle;
}
