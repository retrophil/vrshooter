﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRController : VRGameJamApp
{
    public enum GameState {TITLE,PLAYING,DEAD,SCORE,NAME_ENTRY}
    public GameState gameState;
    void Awake()
    {
       GameLoop(GameState.TITLE);
    }

    public void GameLoop(GameState gameState)
    {

        switch (gameState)
        {
            case GameState.TITLE:
            {
                app.model.health = 1;
                app.model.score = 0;
                app.view.scoreAmount.transform.GetChild(1).GetComponent<Text>().text = app.model.score.ToString();
                app.model.ScoreInt.SetActive(false);
                app.model.ScoreTitle.SetActive(false);
                app.model.yourScoreTitle.SetActive(true);
                app.model.yourScoreInt.SetActive(true);
                app.model.healthObj.SetActive(false);
                app.model.scoreObj.SetActive(false);
                app.model.titleObj.SetActive(true);
                SetEnemy(1,100,30);
                
            }
            break;
            case GameState.PLAYING:
            {
                app.model.health = 1;
                app.view.healthBar.GetComponent<RectTransform>().localScale = new Vector3(app.model.health,1,1);
                app.model.score = 0;
                app.model.titleObj.SetActive(false);
                app.model.healthObj.SetActive(true);
                app.model.scoreObj.SetActive(true);
                app.model.yourScoreTitle.SetActive(false);
                app.model.yourScoreInt.SetActive(false);
                app.model.ScoreInt.SetActive(true);
                app.model.ScoreTitle.SetActive(true);
                ResetEnemies(1,Random.Range(5,10),100);
            }
            break;
            case GameState.DEAD:
            {
                app.model.health = 1;
                app.model.healthObj.SetActive(false);
                app.model.scoreObj.SetActive(true);
                app.model.ScoreTitle.SetActive(false);
                app.model.ScoreInt.SetActive(false);
                app.model.yourScoreInt.SetActive(true);
                app.model.yourScoreTitle.SetActive(true);
                app.model.yourScoreInt.GetComponent<Text>().text = app.model.score.ToString();
                Destroy(app.model.enemiesObj);

                //UI - hide title
                //UI - show gameover screen
                //UI - hide health and score
                //ENEMY - stop enemy speed
                //SPAWNER - stop enemy spawn
            }
            break;
            case GameState.SCORE:
            {
                //UI - hide title
                //SCORE/UI - show scoreboard
                //SCORE/UI - show target
            }
            break;
            case GameState.NAME_ENTRY:
            {
                //UI - show letters
                //UI - show score
                //UI - show name entry space
            }
            break;
        }
    }

    void ResetEnemies(float pace, int speed, float cap)
    {
        //Animation
        //Destroy enemies object
        //Load resources enemies
        Debug.Log("GameSpeed: "+speed);
        Destroy(app.model.enemiesObj);
        SetEnemy(pace,speed,cap);
    }

    void SetEnemy(float pace,int speed, float cap)
    {
        GameObject enemies = Instantiate(Resources.Load("Prefabs/Enemies")) as GameObject;
        app.model.enemiesObj = enemies;
        app.model.enemiesObj.transform.SetParent(app.model.game.transform);
        EnemySpawner eSpawn = app.model.enemiesObj.GetComponent<EnemySpawner>();
        eSpawn.enemyPace = pace;
        eSpawn.setSpeed = speed;
        eSpawn.enemyCap = cap;
    }

    public void SetScore(int scoreUpdate)
    {
        app.model.score += scoreUpdate;
        app.view.scoreAmount.transform.GetChild(1).GetComponent<Text>().text = app.model.score.ToString();
    }

    public void SetHealth(float healthUpdate)
    {
        app.model.health -= healthUpdate;
        app.view.healthBar.GetComponent<RectTransform>().localScale = new Vector3(app.model.health,1,1);
    }
    
}
