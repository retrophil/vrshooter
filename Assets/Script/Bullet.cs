﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    float shotPwr = 5000;
    float timeLife = 0;
    float maxLife = 5;
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * shotPwr);
        
    }

    void Update()
    {
        timeLife += Time.deltaTime;

        if (timeLife > maxLife)
        {
            Destroy(gameObject);
        }
    }

}
