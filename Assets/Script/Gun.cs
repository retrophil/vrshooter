﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Gun : VRGameJamApp
{
    public GameObject bullet;
    public GameObject shotPiece;

    void Update() 
    {
        if (app.controller.gameState == VRController.GameState.TITLE)
        {
            if (Input.GetMouseButtonUp(0))
            {
                app.controller.gameState = VRController.GameState.PLAYING;
                app.controller.GameLoop(VRController.GameState.PLAYING);
            }
        }
        
        if (app.controller.gameState == VRController.GameState.PLAYING)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Instantiate(bullet,shotPiece.transform.position,transform.rotation);
            }
        }

        if (app.controller.gameState == VRController.GameState.DEAD)
        {
            Invoke("DelayPause",2);
        }    
    }

    void DelayPause()
    {
        if (Input.GetMouseButtonUp(0))
            {
                app.controller.gameState = VRController.GameState.TITLE;
                app.controller.GameLoop(VRController.GameState.TITLE);
                SceneManager.LoadScene("VRShooter");
            }
    }
}
