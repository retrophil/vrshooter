﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
     public class VRGameJamApp : MonoBehaviour
    {
        public VRGameJam app {get {return GameObject.FindObjectOfType<VRGameJam>();}}

    }
    public class VRGameJam : MonoBehaviour
    {
        public VRModel model;
        public VRView view;
        public VRController controller;
        
    } 

