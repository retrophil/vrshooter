﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : VRGameJamApp
{
    float spawnRadius = 180;
    public float enemyPace = 1;
    public float enemyCap = 10;
    public int enemyCount = 0;
    public int setSpeed = 0;
    public GameObject enemyType;
    
    void Start()
    {
        Invoke("SpawnEnemy", enemyPace);
    }
    
    void SpawnEnemy()
    {
        if (enemyCount < enemyCap)
        {
            Vector3 spawnWithinCircle = transform.position + ((Vector3)Random.insideUnitCircle).normalized*spawnRadius;
            Vector3 spawnPosition = new Vector3(spawnWithinCircle.x,-15,spawnWithinCircle.y);

            GameObject enemy = Instantiate(enemyType,spawnPosition,enemyType.transform.rotation) as GameObject;
            enemy.transform.SetParent(this.transform);
            enemy.GetComponent<EnemyComponent>().speed = setSpeed;
            Invoke("SpawnEnemy", enemyPace);
            enemyCount++;
        }
        
    }
}
